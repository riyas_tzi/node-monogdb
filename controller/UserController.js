import UserModel from "../model/User.js";
import fs from "fs";

export default {
  getAllUser: async (req, res) => {
    try {
      const users = await UserModel.find();
      res.json({
        data: users,
        message: "users getall successfully",
        status: true,
      });
    } catch (err) {
      res.status(500).json({ error: err.message, status: false });
    }
  },

  createUser: async (req, res) => {
    try {
      const data = JSON.parse(req.body.data);
      data.image = req.file.path;
      await UserModel.create(data);
      res.json({
        message: "user created successfully",
        status: true,
      });
    } catch (err) {
      res.status(500).json({ error: err.message, status: false });
    }
  },

  getUserById: async (req, res) => {
    try {
      const id = req.body.id;
      const user = await UserModel.findById(id);
      res.json({ data: user, status: true, message: "user get successfully" });
    } catch (err) {
      res.status(500).json({ error: err.message, status: false });
    }
  },

  updateUser: async (req, res) => {
    try {
      if (req.file === undefined) {
        const data = JSON.parse(req.body.data);
        const id = data.id;
        // console.log("data",data);
        await UserModel.updateOne({ _id: id }, { $set: data });
        res.json({
          status: true,
          message: "user updated successfully",
        });
      } else {
        const data = JSON.parse(req.body.data);
        data.image = req.file.path;
        const id = data.id;
        const find_user = await UserModel.findById(id);
        const path = find_user.image;
        if (fs.existsSync(path)) {
          fs.unlinkSync(path);
        }
        await UserModel.updateOne({ _id: id }, { $set: data });
        res.json({
          status: true,
          message: "user updated successfully",
        });
      }
    } catch (err) {
      res.status(500).json({ error: err.message, status: false });
    }
  },

  deleteUser: async (req, res) => {
    try {
      const id = req.body.id;
      const find_user = await UserModel.findById(id);
      const path = find_user.image;
      if (fs.existsSync(path)) {
        fs.unlinkSync(path);
        console.log("image deleted");
      }
      await UserModel.findByIdAndDelete(id);
      res.json({
        status: true,
        message: "user deleted successfully",
      });
    } catch (err) {
      res.status(500).json({ error: err.message, status: false });
    }
  },
};
