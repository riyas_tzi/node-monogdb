import multer from "multer";

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        // console.log("fieldname",file.fieldname)
        cb(null, 'public/images/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + "-" + file.originalname)
    }
})
const upload = multer({ storage: storage })
export default upload;