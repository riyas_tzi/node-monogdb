import express from "express";
import bodyParser from "body-parser";
import controller from "../controller/index-controller.js";
import upload from "../middleware/upload.js"
const router = express.Router();


router.get("/getall", controller.UserController.getAllUser);
router.post("/get", bodyParser.urlencoded({extended:true}),controller.UserController.getUserById);
router.post("/create", upload.single('image'), controller.UserController.createUser);
router.post("/update", upload.single('image'), controller.UserController.updateUser);
router.post("/delete", bodyParser.urlencoded({extended:true}),controller.UserController.deleteUser);

export default router;
