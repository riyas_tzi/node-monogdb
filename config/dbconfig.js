// import env from "dotenv";
// import mongodb from "mongodb";
// const { MongoClient, ServerApiVersion } = mongodb;
// env.config();

// const client = new MongoClient(process.env.MONGODB_URI, {
//   useNewUrlParser: true,
//   useUnifiedTopology: true,
//   serverApi: ServerApiVersion.v1,
// });

// async function run() {
//   try {
//     await client.connect();

//     await client.db(process.env.DB_NAME).command({ ping: 1 });
//     console.log(
//       "Pinged your deployment. You successfully connected to MongoDB!"
//     );
//   } finally {
//     await client.close();
//   }
// }
// run().catch(console.dir);

import mongoose from "mongoose";
import env from "dotenv";
import createError from "http-errors";
env.config();

mongoose
  .connect(process.env.MONGODB_URI, {
    dbName: process.env.DB_NAME,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("mongodb is connected");
    console.log(`${process.env.DB_NAME} is created`);
  })
  .catch((err) => {
    throw createError.InternalServerError(err);
  });

mongoose.connection.on("connected", () => {
  console.log("Mongoose connected to MongoDb");
});

mongoose.connection.on("error", (err) => {
  throw createError.InternalServerError(err.message);
});

mongoose.connection.on("disconnected", () => {
  console.log("MongoDb is disconnected");
});
