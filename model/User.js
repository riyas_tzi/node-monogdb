
import mongoose from "mongoose";
const Schema = mongoose.Schema;
 
const blogSchema = new Schema({
  name: String,
  phone: String,
  email:String,
  image: String,
  createdAt: {
    type: Date,
    default: Date.now,
  },
});
 
const Users = mongoose.model("Users", blogSchema);

export default Users;