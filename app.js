import express from "express";
const app = express();
import env from "dotenv";
import cors from "cors";
import bodyParser from "body-parser";
import "./config/dbconfig.js";
import routes from "./routes/index-routes.js";
import path from "path";
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

env.config();
app.use(cors({ origin: "*" }));

//request body is parsed
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// all routes
routes(app);

app.use('/public/images/', express.static(path.join(__dirname, 'public/images/')))

app.listen(process.env.PORT, () => {
  console.log(`🚀 Server started on ${process.env.PORT || 3000}`);
});
